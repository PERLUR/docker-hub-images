# centos-laraplate
The PERLUR boilerplate for a Laravel and Vue.js applications based on [centos-nginx-php-fpm Docker image](https://hub.docker.com/r/perlur/centos-nginx-php-fpm) ([GitHub.com repository](https://github.com/PERLUR/docker-hub-images/tree/master/centos-nginx-php-fpm))

## What's included
### Development tools
* [Laravel](https://laravel.com/)
* [Vue.js](https://vuejs.org/)
* [Composer PHP dependency manager](https://getcomposer.org/)

### Integrated testing tools

### Automated Continuous Integration support
* [StyleCI](https://styleci.io/)
* [TravisCI](https://travis-ci.org/)
* [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)

### Local development tools
* [Docker](https://www.docker.com/)

## Quick start

## License
PERLUR Laraplate is open-sourced and licensed under the [GNU Affero General Public License version 3](https://opensource.org/licenses/AGPL-3.0).
